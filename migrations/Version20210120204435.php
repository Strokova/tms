<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210120204435 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE CUSTOMERS (id INT AUTO_INCREMENT NOT NULL, uid CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_269150CE539B0606 (uid), UNIQUE INDEX customer_id (id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE EXTERNALSERVICES (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX externalservice_id (id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE INTEGRATIONS (id INT AUTO_INCREMENT NOT NULL, customers_id INT DEFAULT NULL, external_services_id INT DEFAULT NULL, INDEX IDX_E91C25BAC3568B40 (customers_id), INDEX IDX_E91C25BAC632EBB6 (external_services_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE TOKENSTATUS (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE TOKENSTORAGE (id INT AUTO_INCREMENT NOT NULL, integrations_id INT DEFAULT NULL, token_status_id INT DEFAULT NULL, value VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_39031AA5A730349E (integrations_id), INDEX IDX_39031AA51F328DFA (token_status_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE INTEGRATIONS ADD CONSTRAINT FK_E91C25BAC3568B40 FOREIGN KEY (customers_id) REFERENCES CUSTOMERS (id)');
        $this->addSql('ALTER TABLE INTEGRATIONS ADD CONSTRAINT FK_E91C25BAC632EBB6 FOREIGN KEY (external_services_id) REFERENCES EXTERNALSERVICES (id)');
        $this->addSql('ALTER TABLE TOKENSTORAGE ADD CONSTRAINT FK_39031AA5A730349E FOREIGN KEY (integrations_id) REFERENCES INTEGRATIONS (id)');
        $this->addSql('ALTER TABLE TOKENSTORAGE ADD CONSTRAINT FK_39031AA51F328DFA FOREIGN KEY (token_status_id) REFERENCES TOKENSTATUS (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE INTEGRATIONS DROP FOREIGN KEY FK_E91C25BAC3568B40');
        $this->addSql('ALTER TABLE INTEGRATIONS DROP FOREIGN KEY FK_E91C25BAC632EBB6');
        $this->addSql('ALTER TABLE TOKENSTORAGE DROP FOREIGN KEY FK_39031AA5A730349E');
        $this->addSql('ALTER TABLE TOKENSTORAGE DROP FOREIGN KEY FK_39031AA51F328DFA');
        $this->addSql('DROP TABLE CUSTOMERS');
        $this->addSql('DROP TABLE EXTERNALSERVICES');
        $this->addSql('DROP TABLE INTEGRATIONS');
        $this->addSql('DROP TABLE TOKENSTATUS');
        $this->addSql('DROP TABLE TOKENSTORAGE');
    }
}
