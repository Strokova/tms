<?php

use App\Kernel;
use bitms\Consul\Service\DatabaseRegister;
use bitms\Consul\Service\KeyValue;
use Symfony\Component\ErrorHandler\Debug;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;

require dirname(__DIR__).'/vendor/autoload.php';

$yaml = Yaml::parseFile(dirname(__FILE__,2) . "/config/packages/consul.yaml");
$config = $yaml["parameters"];

$storage = new KeyValue();

$storageKey = sprintf("%s/%s_%s.json", $config['serviceName'],$config['hostName'],$config['hostPort']);

if (apcu_exists($storageKey)) {
    $storageValue = apcu_fetch($storageKey, $success);
    if (!$success)
        throw new RuntimeException("Failed to get APCU cache value");
}else{
    $storageValue = json_decode($storage->query($storageKey)->getValue());
    apcu_add($storageKey, $storageValue, $config["ttl"]);
}


if ($storageValue->env->APP_DEBUG) {
    umask(0000);
    Debug::enable();
}

$kernel = new Kernel($storageValue->env->APP_ENV, (bool)$storageValue->env->APP_DEBUG);

// If connection to database

if(!empty($storageValue->env->DATABASE)) {
    $connection = new DatabaseRegister();
    $db_params = $connection->setParameters($storageValue->env->DATABASE);
}


$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);