<?php


namespace App\ArgumentResolver;

use App\Entity\ExternalService;
use App\Interfaces\RequestDTOInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

class CreateExternalServiceRequest implements RequestDTOInterface
{

    /**
     * @var integer
     * @Assert\Type(type="int")
     */

    private int $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min=3, max=255)
     */
    private string $name;

    private EntityManagerInterface $em;

    public function __construct(Request $request, EntityManagerInterface $em)
    {
        $this->em = $em;
        $params = json_decode($request->getContent());
        if(!empty($params["id"])){
            $this->id = (int)$params["id"];
        }

        $this->name = (string)$request->request->get("name");
    }

    public function isValid()
    {
        $externalService = new ExternalService();
        $externalService->setName($this->name);

        if(!empty($this->id)) {
            $externalService->setId($this->id);
        }

        return $externalService;

    }


}