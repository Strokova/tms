<?php


namespace App\ArgumentResolver;


use App\Entity\ExternalService;
use App\Interfaces\RequestDTOInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateExternalServiceRequest implements RequestDTOInterface
{

    /**
     * @var integer
     * @Assert\NotBlank()
     */

    private int $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min=3, max=255)
     */
    private string $name;

    private EntityManagerInterface $em;

    public function __construct(Request $request, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->id = (int)$request->request->get("id");
        $this->name = (string)$request->request->get("name");
    }

    public function isValid()
    {
        $externalService = new ExternalService();
        $externalService->setName($this->name);
        $externalService->setId($this->id);

        return $externalService;

    }
}