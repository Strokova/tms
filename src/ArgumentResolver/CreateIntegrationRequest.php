<?php


namespace App\ArgumentResolver;

use App\Entity\Customer;
use App\Entity\ExternalService;
use App\Entity\Integration;
use App\Interfaces\RequestDTOInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class CreateIntegrationRequest implements RequestDTOInterface
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    private string $customer;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    private string $service;

    private array $template = ['customer_id'=>null, "uid"=>null, "service_id" =>null, "name"=>null];

    private EntityManagerInterface $em;

    public function __construct(Request $request, EntityManagerInterface $em)
    {
        $this->em = $em;


        $params = json_decode($request->getContent(), true);
        $params = array_merge($this->template, $params);
        $this->customer = $params["customer_id"] ?? $params["uid"];
        $this->service = $params["service_id"] ?? $params["name"];

    }

    public function isValid()
    {
        $integration = new Integration();

        $customer = $this->em->getRepository(Customer::class)
            ->findOneByIdOrUidOrCreate(['id'=> $this->customer, 'uid' =>$this->customer]);
        $service = $this->em->getRepository(ExternalService::class)
            ->findOneByIdOrNameOrCreate(['id'=>$this->service, 'name'=>$this->service]);

        $integration->setCustomers($customer);
        $integration->setExternalServices($service);

        return $integration;

    }


}