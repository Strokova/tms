<?php


namespace App\ArgumentResolver;

use App\Entity\ExternalService;
use App\Entity\Integration;
use App\Interfaces\RequestDTOInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateIntegrationRequest implements RequestDTOInterface
{

    /**
     * @var integer
     * @Assert\NotBlank()
     */

    private int $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Positive()
     * @Assert\GreaterThan(value="0")
     */
    private int $customer;
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Positive()
     * @Assert\GreaterThan(value="0")
     */
    private int $service;

    private EntityManagerInterface $em;

    public function __construct(Request $request, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->id = (int)$request->request->get("id");
        $this->customer = (int)$request->request->get("customer");
        $this->service = (int)$request->request->get("service");
    }

    public function isValid()
    {
        $integration = new Integration();
        $integration->setId($this->id);
        $integration->setCustomers($this->customer);
        $integration->setExternalServices($this->service);

        return $integration;

    }


}