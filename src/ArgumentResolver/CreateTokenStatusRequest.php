<?php


namespace App\ArgumentResolver;


use App\Entity\TokenStatus;
use App\Interfaces\RequestDTOInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class CreateTokenStatusRequest implements RequestDTOInterface
{
    /**
     * @var int
     * @Assert\Positive
     * @Assert\GreaterThan(value="0")
     */
    private int $id;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(min=3, max=255)
     */
    private string $description;

    private EntityManagerInterface $em;

    public function __construct(Request $request, EntityManagerInterface $em)
    {
        $this->em = $em;
        $params = json_decode($request->getContent(), true);
        if(!empty($params["id"])){
            $this->id = (int)$params['id'];
        }
        $this->description = $params["description"];
    }

    public function isValid()
    {
        $tokenStatus = new TokenStatus();

        if(!empty($this->id)){
            $tokenStatus->setId($this->id);
        }
        $tokenStatus->setDescription($this->description);

        return $tokenStatus;
    }
}