<?php


namespace App\ArgumentResolver;



use App\Entity\TokenStatus;
use App\Interfaces\RequestDTOInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class GetTokenStatusRequest implements RequestDTOInterface
{

    /**
     * @var string
     */
    private string $status;

    private array $template = ["filters" => ["status_id" => null, "status_description"=>null]];


    private EntityManagerInterface $em;

    public function __construct(Request $request, EntityManagerInterface $em)
    {
        $this->em = $em;
        $params = (array)$request->query->all();

        if(isset($params["filters"])){
            $params = array_replace_recursive($this->template, $params);
            $this->status = $params["filters"]["status_id"] ?? $params["filters"]["status_description"];
        }

    }

    public function isValid()
    {
        if(!empty($this->status)) {
            $status = $this->em->getRepository(TokenStatus::class)
                    ->findOneById($this->status) ?? $this->em->getRepository(TokenStatus::class)
                    ->findOneByDescription($this->status);
        }else{
            $status = $this->em->getRepository(TokenStatus::class)
                ->findAll();
        }

        return $status;

    }
}