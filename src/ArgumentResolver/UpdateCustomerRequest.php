<?php


namespace App\ArgumentResolver;


use App\Entity\ExternalService;
use App\Interfaces\RequestDTOInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateCustomerRequest implements RequestDTOInterface
{

    /**
     * @var integer
     * @Assert\NotBlank()
     */

    private int $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Uuid
     */
    private string $uid;

    private EntityManagerInterface $em;

    public function __construct(Request $request, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->id = (int)$request->request->get("id");
        $this->uid = (string)$request->request->get("uid");
    }

    public function isValid()
    {
        $externalService = new ExternalService();
        $externalService->setName($this->uid);
        $externalService->setId($this->id);

        return $externalService;

    }
}