<?php


namespace App\ArgumentResolver;


use App\Entity\Customer;
use App\Interfaces\RequestDTOInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class GetCustomerRequest implements RequestDTOInterface
{
    /**
     * @var string
     */
    private string $customer;

    private array $template = ["filters" => ["customer_id" => null, "customer_uid"=>null]];


    private EntityManagerInterface $em;

    public function __construct(Request $request, EntityManagerInterface $em)
    {
        $this->em = $em;
        $params = (array)$request->query->all();
        $this->customer = '';
        if(isset($params["filters"])){
            $params = array_replace_recursive($this->template, $params);
            $this->customer = $params["filters"]["customer_id"] ?? $params["filters"]["customer_uid"];
        }
    }

    public function isValid()
    {
        if(!empty($this->customer)) {
            $customer = $this->em->getRepository(Customer::class)
                ->findOneById($this->customer) ?? $this->em->getRepository(Customer::class)
                ->findOneByUid($this->customer);
        }elseif($this->customer == ''){
            $customer = '';
        }else{
            $customer = $this->em->getRepository(Customer::class)
                ->findAll();
        }

        return $customer;

    }

}