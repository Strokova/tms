<?php


namespace App\ArgumentResolver;


use App\Entity\Customer;
use App\Interfaces\RequestDTOInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CreateCustomerRequest implements RequestDTOInterface
{

    /**
     * @var integer
     * @Assert\Type(type="int")
     */
    private int $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Uuid()
     */
    private string $uid;

    private EntityManagerInterface $em;

    public function __construct(Request $request, EntityManagerInterface $em)
    {
        $this->em = $em;
        $params = json_decode($request->getContent(), true);

        if(!empty($params['customer_id'])){
            $this->id = (int)$params["customer_id"];
        }
        $this->uid = (string)$params["customer_uid"];
    }

    public function isValid()
    {
        $customer = new Customer();
        $customer->setUid($this->uid);

        if(!empty($this->id)){
            $customer->setId($this->id);
        }

        return $customer;
    }
}