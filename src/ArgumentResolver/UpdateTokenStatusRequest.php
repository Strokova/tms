<?php


namespace App\ArgumentResolver;


use App\Entity\TokenStatus;
use App\Interfaces\RequestDTOInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateTokenStatusRequest implements RequestDTOInterface
{
    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\Positive
     * @Assert\GreaterThan(value="0")
     */
    private int $id;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(min=3, max=255)
     */
    private string $description;

    private EntityManagerInterface $em;

    public function __construct(Request $request, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->id = (int)$request->request->get('id');
        $this->description = (string)$request->request->get('description');
    }

    /**
     * @return TokenStatus
     */
    public function isValid()
    {
        $tokenStatus = new TokenStatus();
        $tokenStatus->setId($this->id);
        $tokenStatus->setDescription($this->description);

        return $tokenStatus;
    }
}