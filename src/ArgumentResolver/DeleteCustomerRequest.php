<?php


namespace App\ArgumentResolver;


use App\Interfaces\RequestDTOInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class DeleteCustomerRequest implements RequestDTOInterface
{
    /**
     * @var int
     * @Assert\NotBlank()
     */
    private int $id;

    private EntityManagerInterface $em;

    public function __construct(Request $request, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->id = (int)$request->request->get("id");
    }

    public function isValid()
    {
        return $this->id;
    }
}