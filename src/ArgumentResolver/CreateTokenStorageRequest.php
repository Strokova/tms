<?php


namespace App\ArgumentResolver;


use App\Entity\TokenStorage;
use App\Interfaces\RequestDTOInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class CreateTokenStorageRequest implements RequestDTOInterface
{
    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\Positive
     * @Assert\GreaterThan(value="0")
     */
    private int $id;

    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\Positive
     * @Assert\GreaterThan(value="0")
     */
    private int $integration;

    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\Positive
     * @Assert\GreaterThan(value="0")
     */
    private int $tokenStatus;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    private string $value;

    private EntityManagerInterface $em;

    public function __construct(Request $request, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->id = (int)$request->request->get('id');
        $this->integration = (int)$request->request->get("integration");
        $this->tokenStatus = (int)$request->request->get("status");
        $this->value = (string)$request->request->get('value');
    }

    public function isValid()
    {
        $tokenStorage = new TokenStorage();
        $tokenStorage->setId($this->id);
        $tokenStorage->setIntegrations($this->integration);
        $tokenStorage->setTokenStatus($this->tokenStatus);

        return $tokenStorage;
    }
}