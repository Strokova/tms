<?php


namespace App\ArgumentResolver;


use App\Entity\ExternalService;
use App\Interfaces\RequestDTOInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class GetExternalServiceRequest implements RequestDTOInterface
{

    /**
     * @var string
     */
    private string $service;

    private array $template = ["filters" => ["service_id" => null, "service_name"=>null]];


    private EntityManagerInterface $em;

    public function __construct(Request $request, EntityManagerInterface $em)
    {
        $this->em = $em;
        $params = (array)$request->query->all();
        $this->service = '';
        if(isset($params["filters"])){
            $params = array_replace_recursive($this->template, $params);
            $this->service = $params["filters"]["service_id"] ?? $params["filters"]["service_name"];
        }
    }

    public function isValid()
    {
        if(!empty($this->service)) {
            $service = $this->em->getRepository(ExternalService::class)
                    ->findOneById($this->service) ?? $this->em->getRepository(ExternalService::class)
                    ->findOneByName($this->service);
        }elseif($this->service == ''){
            $service = '';
        }else{
            $service = $this->em->getRepository(ExternalService::class)
                ->findAll();
        }

        return $service;

    }
}