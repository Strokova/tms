<?php


namespace App\ArgumentResolver;



use App\Entity\Integration;
use App\Entity\TokenStatus;
use App\Interfaces\RequestDTOInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;

class GetIntegrationRequest implements RequestDTOInterface
{
    /**
     * @var string
     */
    private string $integration;

    /**
     * @var string
     */
    private string $service;

    /**
     * @var string
     */
    private string $customer;

    /**
     * @var array|\null[][]
     */
    private array $template = ["filters" =>
        [
            "integration_id" => null,
            "service_id" => null,
            "service_name" => null,
            "customer_id" => null,
            "customer_uid"=>null
        ]
    ];


    private EntityManagerInterface $em;

    public function __construct(Request $request, EntityManagerInterface $em)
    {
        $this->em = $em;
        $params = (array)$request->query->all();

        if(isset($params["filters"])){
            $params = array_replace_recursive($this->template, $params);

            $this->integration = $params["filters"]["integration_id"] ?? '';
            $this->service = $params["filters"]["service_id"] ?? $params["filters"]["service_name"];
            $this->customer = $params["filters"]["customer_id"] ?? $params["filters"]["customer_uid"];
        }
    }

    public function isValid()
    {
        if(!empty($this->integration)) {
            $integration = $this->em->getRepository(Integration::class)
                    ->findOneById($this->integration);
        }elseif(!empty($this->service) && empty($this->customer)) {
//            dump(['customers'=>$this->customer, 'externalServices'=>$this->service]);
            $integration = $this->em->getRepository(Integration::class)
                ->findByExternalServices($this->service);
        }elseif(!empty($this->customer) && empty($this->service)){
//            dump(['customers'=>$this->customer, 'externalServices'=>$this->service]);
            $integration = $this->em->getRepository(Integration::class)
                ->findByCustomers($this->customer);
        }elseif(!empty($this->customer) && !empty($this->service)){
//            dump(['customers'=>$this->customer, 'externalServices'=>$this->service]);
            $integration = $this->em->getRepository(Integration::class)
                ->findOneByCustomerAndServices($this->customer, $this->service);
        }else{
            $integration = $this->em->getRepository(Integration::class)
                ->findAll();
        }

        return $integration;

    }
}