<?php

namespace App\Builders;

use App\Entity\Customer;

class CustomerBuilder
{
    private Customer $customer;

    public function reset()
    {
        $this->customer = new Customer();
    }

    public function get($param)
    {

    }

    public function setData(array $data)
    {
        $this->customer->setUid($data["uid"]);
    }


}