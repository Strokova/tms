<?php


namespace App\Controller;


use App\ArgumentResolver\CreateIntegrationRequest;
use App\ArgumentResolver\DeleteIntegrationRequest;
use App\ArgumentResolver\GetIntegrationRequest;
use App\ArgumentResolver\UpdateIntegrationRequest;
use App\Entity\Integration;
use App\Traits\IsExisting;
use App\Traits\SetResult;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class IntegrationController extends AbstractController
{
    use SetResult, IsExisting;

    /**
     * @param CreateIntegrationRequest $request
     * @return JsonResponse
     *
     * @Route(path="integration/add", name="integration_create", methods={"POST"})
     */
    public function create(CreateIntegrationRequest $request): JsonResponse
    {
        try{
            $integration = $request->isValid();

            if (isset($request->errors)) {
                $result = $this->setResult($request->errors, 400);
            }else{

                $existing = $this->isExisits(Integration::class,
                    [
                        'customers' => $integration->getCustomers()->getId(),
                        'externalServices'=>$integration->getExternalServices()->getId()
                    ]);

                if($existing == false) {
                    $this->getDoctrine()->getRepository(Integration::class)->save($integration);
                    $result = $this->setResult($integration, 201);
                }else{
                    $result = $this->setResult($existing, 200);
                }
            }
        }catch (\Exception $exception){
            $result = $this->setResult($exception->getMessage(), 400);
        }
            return new JsonResponse($result->message, $result->code);
    }

    /**
     * @param UpdateIntegrationRequest $request
     * @param SerializerInterface $serializer
     * @return JsonResponse
     *
     * @Route(path="integration/update", name="integration_update", methods={"POST"})
     */
    public function update(UpdateIntegrationRequest $request, SerializerInterface $serializer)
    {
        $om = $this->getDoctrine()->getManager();
        $result = $request->isValid();

//        $customer = $om->find(Customer::class, $result->getCustomers());
//        $service = $om->find(ExternalService::class, $result->getExternalServices());
        $integration = $om->find(Integration::class, $result->getId());

        if(empty($customer) || empty($service)){
            return new JsonResponse("customer or servisr does not exists", 400);
        }

        $integration->setExternalService($result->getExternalServices());
        $integration->setCustomers($result->getCustomers());

        $om->merge($integration);
        $om->flush();

        $result = $serializer->serialize($integration, 'json');

        return new JsonResponse($integration, 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route(path="integration/get", name="integration_get", methods={"GET"})
     */
    public function getIntegration(GetIntegrationRequest $request)
    {
        try {
            $integration = $request->isValid();
            $data = $this->serializer->serialize($integration,'json');
            $result = $this->setResult("Integration(s) not found", 400);
            if (!empty($integration)) {
                $result = $this->setResult($integration, 200);
            }
        }catch (\Exception $exception){
            $result = $this->setResult($exception->getMessage(), 400);
        }

        return new JsonResponse($result, 200);
    }

    /**
     * @param DeleteIntegrationRequest $request
     * @param SerializerInterface $serializer
     * @return string|JsonResponse
     *
     * @Route(path="service/remove", name="integration_remove", methods={"POST"})
     */
    public function delete(DeleteIntegrationRequest $request, SerializerInterface $serializer)
    {
        $om = $this->getDoctrine()->getManager();

        $id = $request->isValid();

        if (!empty($request->errors)) {
            return new JsonResponse($request->errors, 400);
        }

        $integration = $om->find(Integration::class, $id);

        $om->remove($integration);
        $om->flush();

        $integration = $serializer->serialize($integration, 'json');

        return $integration;
    }
}