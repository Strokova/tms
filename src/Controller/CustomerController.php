<?php


namespace App\Controller;


use App\ArgumentResolver\CreateCustomerRequest;
use App\ArgumentResolver\DeleteCustomerRequest;
use App\ArgumentResolver\GetCustomerRequest;
use App\ArgumentResolver\UpdateCustomerRequest;
use App\Entity\Customer;
use App\Traits\IsExisting;
use App\Traits\SetResult;
use http\Client\Curl\User;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CustomerController extends AbstractController
{
    use SetResult, IsExisting;

    private $serializer;

    public function __construct()
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    /**
     * @param CreateCustomerRequest $request
     * @return JsonResponse
     *
     * @Route(path="customer/add", name="customer_create", methods={"POST"})
     */
    public function create(CreateCustomerRequest $request)
    {
        try {
            $customer = $request->isValid();

            if (!empty($request->errors)) {
                $result = $this->setResult($request->errors, 400);
            }else{
                $existing = $this->isExisits(Customer::class, ['uid' => $customer->getUid()]);
                if($existing == false) {
                    $this->getDoctrine()->getRepository(Customer::class)->save($customer);
                    $result = $this->setResult($customer, 201);
                }else{
                    $result = $this->setResult($existing, 200);
                }
            }
        }catch (\Exception $exception){
            $result = $this->setResult($exception->getMessage(), 400);
        }

        return new JsonResponse($result->message, $result->code);
    }

    /**
     * @param UpdateCustomerRequest $request
     * @param SerializerInterface $serializer
     * @return JsonResponse
     *
     * @Route(path="customer/update", name="customer_update", methods={"POST"})
     */
    public function update(UpdateCustomerRequest $request, SerializerInterface $serializer)
    {
        $om = $this->getDoctrine()->getManager();

        $result = $request->isValid();

        if (!empty($request->errors)) {
            return new JsonResponse($request->errors, 400);
        }

        $customer = $om->find(Customer::class, $result->getId());

        if(!empty($customer)){
            $customer->setName($result->getName());
        }

        $om->merge($customer);
        $om->flush();

        $customer = $serializer->serialize($customer, 'json');

        return new JsonResponse($customer, 200);
    }

    /**
     * @param GetCustomerRequest $request
     * @return JsonResponse
     *
     * @Route(path="customer/get", name="customer_get", methods={"GET"})
     */
    public function getCustomer(GetCustomerRequest $request)
    {
        try {
            $customers = $request->isValid();
            $result = $this->setResult("Customer(s) not found", 400);
            if (!empty($customers)) {
                $result = $this->setResult($customers, 200);
            }
        }catch (\Exception $exception){
            $result = $this->setResult($exception->getMessage(), 400);
        }

        return new JsonResponse($result, 200);
    }

    /**
     * @param DeleteCustomerRequest $request
     * @param SerializerInterface $serializer
     * @return string|JsonResponse
     *
     * @Route(path="service/remove", name="external_service_remove", methods={"POST"})
     */
    public function delete(DeleteCustomerRequest $request, SerializerInterface $serializer)
    {
        $om = $this->getDoctrine()->getManager();

        $id = $request->isValid();

        if (!empty($request->errors)) {
            return new JsonResponse($request->errors, 400);
        }

        $customer = $om->find(Customer::class, $id);

        $om->remove($customer);
        $om->flush();

        $customer = $serializer->serialize($customer, 'json');

        return $customer;
    }
}