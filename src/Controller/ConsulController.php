<?php


namespace App\Controller;

use bitms\Consul\Model\Service\AgentCheck;
use bitms\Consul\Model\ServiceModel;
use bitms\Consul\Service\RegisterService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class ConsulController extends AbstractController
{
    private RegisterService $service;

    public function __construct()
    {
        $this->service = new RegisterService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route(path="consul/register",name="set_register", methods={"GET"})
     */
    public function setRegister(Request $request): JsonResponse
    {
        $serviceName = $this->getParameter('serviceName');
        $hostName = $this->getParameter('hostName');
        $hostPort = $this->getParameter('hostPort');

        $storageKey = sprintf("%s/%s_%s.json", $serviceName,$hostName,$hostPort);
        $storageValue = apcu_fetch($storageKey, $success);
        $consul = $storageValue->consul;
        $service = new ServiceModel();
        $service->setAddress($consul->Address);
        $service->setName($consul->Name);
        $service->setPort($consul->Port);
        $service->setTag($consul->Tags);
        $service->setId($consul->ID);
        $check = new AgentCheck();
        $check->setName('HTTP API on port 80');
        $check->setTimeout('1s');

        $check->setHeader([
            "Content-Type"=>"application/json"
        ]);

        $check->setDeregister('90m');
        $checkUri = sprintf('http://%s:%s/consul/health', $consul->Address, $consul->Port);
        $check->setHttp($checkUri);
        $check->setInterval("10s");
        $check->setTls(false);
        $check->setMethod("POST");
        $check->setBody(["method"=>"health"]);
        $service->setChecks($check);
        $result =  $this->service->install($service);
        return new JsonResponse(['success' => $result],200);
    }

    /**
     * @param Request $request
     * @Route(path="consul/health", methods={"POST"})
     * @return JsonResponse
     */
    public function health(Request $request): JsonResponse
    {
        return $this->json('ok');
    }

}
