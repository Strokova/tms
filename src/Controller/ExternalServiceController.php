<?php

namespace App\Controller;

use App\ArgumentResolver\CreateExternalServiceRequest;
use App\ArgumentResolver\DeleteExternalServiceRequest;
use App\ArgumentResolver\GetExternalServiceRequest;
use App\ArgumentResolver\UpdateExternalServiceRequest;
use App\Entity\ExternalService;
use App\Traits\IsExisting;
use App\Traits\SetResult;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class ExternalServiceController extends AbstractController
{
    use SetResult, IsExisting;
    /**
     * @param CreateExternalServiceRequest $request
     * @return JsonResponse
     * @Route(path="service/add", name="external_service_create", methods={"POST"})
     */
    public function create(CreateExternalServiceRequest $request): JsonResponse
    {
        try {
            $externalService = $request->isValid();

            if (!empty($request->errors)) {
                $result = $this->setResult($request->errors, 400);
            }else{

                $existing = $this->isExisits(ExternalService::class, ['name' => $externalService->getName()]);
                if($existing === null) {
                    $this->getDoctrine()->getRepository(ExternalService::class)->save($externalService);
                    $result = $this->setResult($externalService, 201);
                }else{
                    $result = $this->setResult($existing, 200);
                }
            }
        }catch (\Exception $exception){
            $result = $this->setResult($exception->getMessage(), 400);
        }

        return new JsonResponse($result->message, $result->code);
    }

    /**
     * @param UpdateExternalServiceRequest $request
     * @param SerializerInterface $serializer
     * @return JsonResponse
     *
     * @Route(path="service/update", name="external_service_update", methods={"POST"})
     */
    public function update(UpdateExternalServiceRequest $request, SerializerInterface $serializer)
    {
        $om = $this->getDoctrine()->getManager();
        $result = $request->isValid();

        if (!empty($request->errors)) {
            return new JsonResponse($request->errors, 400);
        }

        $externalService = $om->find(ExternalService::class, $result->getId());

        if(!empty($externalService)){
            $externalService->setName($result->getName());
        }

        $om->merge($externalService);
        $om->flush();

        $result = $serializer->serialize($result, 'json');

        return new JsonResponse($result, 200);
    }

    /**
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return JsonResponse
     *
     * @Route(path="service/get", name="external_service_get", methods={"GET"})
     */
    public function getServices(GetExternalServiceRequest $request)
    {
        try {
            $services = $request->isValid();
            $result = $this->setResult("Service(s) not found", 400);
            if (!empty($services)) {
                $result = $this->setResult($services, 200);
            }
        }catch (\Exception $exception){
            $result = $this->setResult($exception->getMessage(), 400);
        }
        return new JsonResponse($result, 200);
    }

    /**
     * @param DeleteExternalServiceRequest $request
     * @param SerializerInterface $serializer
     * @return string|JsonResponse
     *
     * @Route(path="service/remove", name="external_service_remove", methods={"POST"})
     */
    public function delete(DeleteExternalServiceRequest $request, SerializerInterface $serializer)
    {
        $om = $this->getDoctrine()->getManager();

        $id = $request->isValid();

        if (!empty($request->errors)) {
            return new JsonResponse($request->errors, 400);
        }

        $externalService = $om->find(ExternalService::class, $id);

        $om->remove($externalService);
        $om->flush();

        $externalService = $serializer->serialize($externalService, 'json');

        return $externalService;
    }
}