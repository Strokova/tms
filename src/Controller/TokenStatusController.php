<?php


namespace App\Controller;


use App\ArgumentResolver\CreateTokenStatusRequest;
use App\ArgumentResolver\DeleteTokenStatusRequest;
use App\ArgumentResolver\GetTokenStatusRequest;
use App\ArgumentResolver\UpdateTokenStatusRequest;
use App\Entity\Customer;
use App\Entity\TokenStatus;
use App\Traits\IsExisting;
use App\Traits\SetResult;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class TokenStatusController extends AbstractController
{
    use SetResult, IsExisting;
    /**
     * @param CreateTokenStatusRequest $request
     * @return JsonResponse
     *
     * @Route(path="status/add", name="token_status_create", methods={"POST"})
     */
    public function create(CreateTokenStatusRequest $request)
    {
        try {
            $tokenStatus = $request->isValid();
            if (!empty($request->errors)) {
                $result = $this->setResult($request->errors, 400);
            }else{
                $existing = $this->isExisits(TokenStatus::class,
                    ['description' => $tokenStatus->getDescription()]);
                if($existing == false) {
                    $this->getDoctrine()->getRepository(TokenStatus::class)->save($tokenStatus);
                    $result = $this->setResult($tokenStatus, 201);
                }else{
                    $result = $this->setResult($existing, 200);
                }
            }
        }catch (\Exception $exception){
            $result = $this->setResult($exception->getMessage(), 400);
        }

        return new JsonResponse($result->message, $result->code);
    }

    /**
     * @param UpdateTokenStatusRequest $request
     * @param SerializerInterface $serializer
     * @return JsonResponse
     *
     * @Route(path="status/update", name="token_status_update", methods={"POST"})
     */
    public function update(UpdateTokenStatusRequest $request,SerializerInterface $serializer)
    {
        $om = $this->getDoctrine()->getManager();

        $result = $request->isValid();

        if (!empty($request->errors)) {
            return new JsonResponse($request->errors, 400);
        }

        $tokenStatus = $om->find(TokenStatus::class, $result->getId());

        if(!empty($tokenStatus)){
            $tokenStatus->setDescription($result->getDescription());
        }

        $om->merge($tokenStatus);
        $om->flush();

        $tokenStatus = $serializer->serialize($tokenStatus, 'json');

        return new JsonResponse($tokenStatus, 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route(path="status/get", name="token_status_get", methods={"GET"})
     */
    public function getStatus(GetTokenStatusRequest $request)
    {
        try {
            $status = $request->isValid();
            $result = $this->setResult("Status(es) not found", 400);
            if (!empty($status)) {
                $result = $this->setResult($status, 200);
            }
        }catch (\Exception $exception){
            $result = $this->setResult($exception->getMessage(), 400);
        }
        return new JsonResponse($result, 200);
    }

    /**
     * @param DeleteTokenStatusRequest $request
     * @param SerializerInterface $serializer
     * @return string|JsonResponse
     *
     * @Route(path="status/remove", name="token_status_delete", methods={"POST"})
     */
    public function delete(DeleteTokenStatusRequest $request, SerializerInterface $serializer)
    {
        $om = $this->getDoctrine()->getManager();

        $id = $request->isValid();

        if (!empty($request->errors)) {
            return new JsonResponse($request->errors, 400);
        }

        $tokenStatus = $om->find(TokenStatus::class, $id);

        $om->remove($tokenStatus);
        $om->flush();

        $tokenStatus = $serializer->serialize($tokenStatus, 'json');

        return $tokenStatus;
    }
}