<?php


namespace App\Controller;


use App\ArgumentResolver\CreateTokenStorageRequest;
use App\ArgumentResolver\DeleteTokenStorageRequest;
use App\ArgumentResolver\UpdateTokenStorageRequest;
use App\Entity\Integration;
use App\Entity\TokenStatus;
use App\Entity\TokenStorage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class TokenStorageController extends AbstractController
{
    /**
     * @param CreateTokenStorageRequest $request
     * @param SerializerInterface $serializer
     * @return JsonResponse
     *
     * @Route(path="storage/create", name="token_storage_create", methods={"POST"})
     */
    public function create(CreateTokenStorageRequest $request, SerializerInterface $serializer)
    {
        $om = $this->getDoctrine()->getManager();
        $result = $request->isValid();

        if (!empty($request->errors)) {
            return new JsonResponse($request->errors, 400);
        }

        $integration = $om->find(Integration::class, $result->getIntegrations());
        $status = $om->find(TokenStatus::class, $result->getTokenStatus());

        if(empty($integration) || empty($status)){
            return new JsonResponse("integration or status does not exists", 400);
        }

        $om->persist($result);
        $om->flush();

        $result = $serializer->serialize($result, 'json');

        return new JsonResponse($result, 200);
    }

    /**
     * @param UpdateTokenStorageRequest $request
     * @param SerializerInterface $serializer
     * @return JsonResponse
     *
     * @Route(path="storage/update", name="token_storage_update", methods={"POST"})
     */
    public function update(UpdateTokenStorageRequest $request, SerializerInterface $serializer)
    {
        $om = $this->getDoctrine()->getManager();
        $result = $request->isValid();

        $integration = $om->find(Integration::class, $result->getIntegrations());
        $status = $om->find(TokenStatus::class, $result->getTokenStatus());
        $storage = $om->find(TokenStorage::class, $result->getId());

        if(empty($integration) || empty($status)){
            return new JsonResponse("integration or status does not exists", 400);
        }

        $storage->setIntegrations($integration);
        $storage->setTokenStatus($status);

        $om->merge($storage);
        $om->flush();

        $storage = $serializer->serialize($storage, 'json');

        return new JsonResponse($storage, 200);
    }

    /**
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return JsonResponse
     *
     * @Route(path="storage/get", name="token_storage_get", methods={"GET"})
     */
    public function getStorage(Request $request, SerializerInterface $serializer)
    {
        $om = $this->getDoctrine()->getManager();
        $filters = $request->query->all();

        $data = "filtered data";

        return new JsonResponse($data, 200);
    }

    /**
     * @param DeleteTokenStorageRequest $request
     * @param SerializerInterface $serializer
     * @return object|JsonResponse|null
     *
     * @Route(path="storage/remove", name="token_storage_delete", methods={"POST"})
     */
    public function delete(DeleteTokenStorageRequest $request,SerializerInterface $serializer)
    {
        $om = $this->getDoctrine()->getManager();

        $id = $request->isValid();

        if (!empty($request->errors)) {
            return new JsonResponse($request->errors, 400);
        }

        $tokenStorage = $om->find(TokenStatus::class, $id);

        $om->remove($tokenStorage);
        $om->flush();

        $tokenStorage = $serializer->serialize($tokenStorage, 'json');

        return $tokenStorage;
    }
}