<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use App\Repository\IntegrationRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @package App\Entity
 * @ORM\Entity(repositoryClass=IntegrationRepository::class)
 * @ORM\Table(name="INTEGRATIONS")
 * @ORM\HasLifecycleCallbacks()
 */
class Integration
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TokenStorage", mappedBy="integrations")
     */
    private Collection $tokenStorages;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", inversedBy="integrations")
     * @ORM\JoinColumn(name="customers_id", referencedColumnName="id")
     */
    private $customers;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ExternalService", inversedBy="integrations")
     * @ORM\JoinColumn(name="external_services_id", referencedColumnName="id")
     */
    private  $externalServices;

    public function __construct()
    {
        $this->tokenStorages = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Collection
     */
    public function getTokenStorages(): Collection
    {
        return $this->tokenStorages;
    }

    /**
     * @param Collection $tokenStorages
     */
    public function setTokenStorages(Collection $tokenStorages): void
    {
        $this->tokenStorages = $tokenStorages;
    }

    /**
     * @return mixed
     */
    public function getCustomers()
    {
        return $this->customers;
    }

    /**
     * @param mixed $customers
     */
    public function setCustomers($customers): void
    {
        $this->customers = $customers;
    }

    /**
     * @return mixed
     */
    public function getExternalServices()
    {
        return $this->externalServices;
    }

    /**
     * @param mixed $externalServices
     */
    public function setExternalServices($externalServices): void
    {
        $this->externalServices = $externalServices;
    }
}