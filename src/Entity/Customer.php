<?php

namespace App\Entity;

use App\Traits\Entity\WithTimestamp;
use App\Repository\CustomerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Uid\AbstractUid;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @package App\Entity
 * @ORM\Entity(repositoryClass=CustomerRepository::class)
 * @ORM\Table(name="CUSTOMERS", uniqueConstraints={@ORM\UniqueConstraint(name="uid", columns={"uid"})})
 * @UniqueEntity(
 *     fields={"uid"},
 *     errorPath="uid",
 *     message="Customer already exists"
 * )
 * @ORM\HasLifecycleCallbacks()
 */
class Customer
{
    use WithTimestamp;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="guid", unique=true, nullable=false, unique=true)
     * @Assert\NotBlank(payload={"severity"="error"})
     * @Assert\Uuid()App\Entity\Integration
     */
    private $uid;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Integration", mappedBy="customers")
     */
    private $integrations;

    public function __construct()
    {
        $this->integrations = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return AbstractUid|string $uid
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param AbstractUid|string $uid
     */
    public function setUid($uid): void
    {
        $this->uid = $uid;
    }

    /**
     * @return ArrayCollection
     */
    public function getIntegrations()
    {
        return $this->integrations;
    }

    /**
     * @param Integration $integrations
     */
    public function setIntegrations(Integration $integrations): void
    {
        $this->integrations = $integrations;
    }

}