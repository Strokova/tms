<?php

namespace App\Entity;

use App\Traits\Entity\WithTimestamp;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\TokenStorageRepository;

/**
 * @package App\Entity
 * @ORM\Entity(repositoryClass=TokenStorageRepository::class)
 * @ORM\Table(name="TOKENSTORAGE")
 * @ORM\HasLifecycleCallbacks()
 */
class TokenStorage
{
    use WithTimestamp;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Integration", inversedBy="tokenStorages")
     * @ORM\JoinColumn(name="integrations_id", referencedColumnName="id")
     */
    private $integrations;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TokenStatus", inversedBy="tokenStorages")
     * @ORM\JoinColumn(name="token_status_id", referencedColumnName="id")
     */
    private $tokenStatus;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTokenStatus()
    {
        return $this->tokenStatus;
    }

    /**
     * @param mixed $tokenStatus
     */
    public function setTokenStatus($tokenStatus): void
    {
        $this->tokenStatus = $tokenStatus;
    }

    /**
     * @return mixed
     */
    public function getIntegrations()
    {
        return $this->integrations;
    }

    /**
     * @param mixed $integrations
     */
    public function setIntegrations($integrations): void
    {
        $this->integrations = $integrations;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }
}