<?php

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\TokenStatusRepository;

/**
 * @package App\Entity
 * @ORM\Entity(repositoryClass=TokenStatusRepository::class)
 * @ORM\Table(name="TOKENSTATUS")
 * @ORM\HasLifecycleCallbacks()
 */
class TokenStatus
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TokenStorage", mappedBy="tokenStatus")
     */
    private Collection $tokenStorages;

    public function __construct()
    {
        $this->tokenStorages = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return Collection
     */
    public function getTokenStorages()
    {
        return $this->tokenStorages;
    }

    /**
     * @param Collection $tokenStorages
     */
    public function setTokenStorages($tokenStorages): void
    {
        $this->tokenStorages = $tokenStorages;
    }
}