<?php

namespace App\Entity;

use App\Traits\Entity\WithTimestamp;
use App\Repository\ExternalServiceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @package App\Entity
 * @ORM\Entity(repositoryClass=ExternalServiceRepository::class)
 * @ORM\Table(name="EXTERNALSERVICES",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="externalservice_id", columns={"id"})})
 * @ORM\HasLifecycleCallbacks()
 */
class ExternalService
{
    use WithTimestamp;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Integration", mappedBy="externalServices")
     */
    private Collection $integrations;

    public function __construct()
    {
        $this->integrations = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Collection
     */
    public function getIntegrations(): Collection
    {
        return $this->integrations;
    }

    /**
     * @param Collection $integrations
     */
    public function setIntegrations(Collection $integrations): void
    {
        $this->integrations = $integrations;
    }

    /**
     * @param Integration $integration
     */
    public function addIntegration(Integration $integration)
    {
        $this->integrations->add($integration);
    }
}