<?php


namespace App\Interfaces;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

interface RequestDTOInterface
{
    public function __construct(Request $request, EntityManagerInterface $em);
}