<?php

namespace App;

use bitms\Consul\Service\DatabaseRegister;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use Symfony\Component\Yaml\Yaml;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    protected function configureContainer(ContainerConfigurator $container): void
    {
        $yaml = Yaml::parseFile(dirname(__FILE__,2) . "/config/packages/consul.yaml");

        $storageKey = sprintf('%s/%s_%s.json', $yaml['parameters']['serviceName'],
            $yaml['parameters']['hostName'], $yaml['parameters']['hostPort']);

        $storageValue = apcu_fetch($storageKey, $success);
        if(!empty($storageValue->env)) {
            $connection = new DatabaseRegister;
            $db_params = $connection->setParameters($storageValue->env->DATABASE);
            if (!empty($db_params)) {
                $container->extension('doctrine', $db_params);
            }
        }

        $container->import('../config/{packages}/*.yaml');
        $container->import('../config/{packages}/'.$this->environment.'/*.yaml');

        if (is_file(\dirname(__DIR__).'/config/services.yaml')) {
            $container->import('../config/services.yaml');
            $container->import('../config/{services}_'.$this->environment.'.yaml');
        } elseif (is_file($path = \dirname(__DIR__).'/config/services.php')) {
            (require $path)($container->withPath($path), $this);
        }

    }

    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $routes->import('../config/{routes}/'.$this->environment.'/*.yaml');
        $routes->import('../config/{routes}/*.yaml');

        if (is_file(\dirname(__DIR__).'/config/routes.yaml')) {
            $routes->import('../config/routes.yaml');
        } elseif (is_file($path = \dirname(__DIR__).'/config/routes.php')) {
            (require $path)($routes->withPath($path), $this);
        }
    }
}
