<?php


namespace App\Traits\Entity;


use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

trait WithTimestamp
{
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private  $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private  $updated_at;

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        if ($this->created_at === null) {
            // TODO Handle this exception.
            throw new \UnexpectedValueException('Property created_at must not be getting being empty');
        }
        return $this->created_at;
    }

    /**
     * @param DateTimeInterface $created_at
     */
    public function setCreatedAt(DateTimeInterface $created_at): void
    {
        $this->created_at = $created_at;
    }

    /**
     * @return DateTimeInterface
     */
    public function getUpdatedAt(): DateTimeInterface
    {
        if ($this->updated_at === null) {
            // TODO Handle this exception.
            throw new \UnexpectedValueException('Property updated_at must not be getting being empty');
        }
        return $this->updated_at;
    }

    /**
     * @param DateTimeInterface $updated_at
     */
    public function setUpdatedAt(DateTimeInterface $updated_at): void
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updatedTimestamps(): void
    {
        $this->updated_at = new DateTimeImmutable('now');
        if ($this->created_at === null) {
            $this->created_at = new DateTimeImmutable('now');
        }
    }
}
