<?php


namespace App\Traits;


trait RepositoryFunctions
{
    /**
     * @param $entity
     */
    public function save($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    /**
     * @param $entity
     */
    public function update($entity)
    {
        $this->em->merge($entity);
        $this->em->flush();
    }
}