<?php


namespace App\Traits;


use Doctrine\ORM\EntityManagerInterface;
use Exception;
use JMS\Serializer\SerializerBuilder;

trait IsExisting
{
    private EntityManagerInterface $em;

    private function isExisits($repository, array $criteria)
    {
        $entity = $this->getDoctrine()->getRepository($repository)
            ->findOneBy($criteria);
        if (empty($entity)) {
            return false;
        }
        return $entity;
    }
}