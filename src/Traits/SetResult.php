<?php


namespace App\Traits;


use JMS\Serializer\SerializerBuilder;
use stdClass;

trait SetResult
{

    private $serializer;

    public function __construct()
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    /**
     * @param $message
     * @param int $code
     * @return stdClass
     */
    public function setResult($message, int $code): stdClass
    {
        $result = new stdClass();
        $result->message = $this->serializer->serialize($message,'json');
        $result->code = $code;

        return $result;
    }
}