<?php


namespace App\Repository;


use App\Entity\TokenStatus;
use App\Traits\RepositoryFunctions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TokenStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method TokenStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method TokenStatus[]    findAll()
 * @method TokenStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method TokenStatus|null save($entity)
 * @method TokenStatus|null update($entity)
 */
class TokenStatusRepository extends ServiceEntityRepository
{
    use RepositoryFunctions;

    private EntityManagerInterface $em;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em)
    {
        parent::__construct($registry, TokenStatus::class);
        $this->em = $em;
    }
}