<?php

namespace App\Repository;


use App\Entity\Customer;
use App\Entity\ExternalService;
use App\Traits\RepositoryFunctions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;


/**
 * @method Customer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Customer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Customer[]    findAll()
 * @method Customer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Customer|null save($entity)
 * @method Customer|null update($entity)
 */
class CustomerRepository extends ServiceEntityRepository
{
    use RepositoryFunctions;

    private EntityManagerInterface $em;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em)
    {
        parent::__construct($registry, Customer::class);
        $this->em = $em;
    }

    /**
     * @param array $criteria
     * @return mixed
     */
    public function findOneByIdOrUidOrCreate(array $criteria)
    {
        $qb = $this->em->createQueryBuilder();
        $customer = $qb->select(array("c")) // string 'u' is converted to array internally
        ->from(Customer::class, 'c')
        ->where($qb->expr()->orX(
            $qb->expr()->eq('c.id', '?1'),
            $qb->expr()->eq('c.uid', '?2')
        ))
        ->setParameters(array(1 => $criteria["id"], 2 => $criteria['uid']))
        ->getQuery()
        ->getOneOrNullResult(AbstractQuery::HYDRATE_OBJECT);


        if(empty($customer)){
            $entity = new Customer();

            $entity->setUid($criteria["uid"]);
            $this->save($entity);

            $customer=$entity;
        }

        return $customer;
    }

}