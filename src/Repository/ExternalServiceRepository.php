<?php


namespace App\Repository;


use App\Entity\ExternalService;
use App\Traits\RepositoryFunctions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExternalService|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExternalService|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExternalService[]    findAll()
 * @method ExternalService[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method ExternalService|null save($entity)
 * @method ExternalService|null update($entity)
 */
class ExternalServiceRepository extends ServiceEntityRepository
{
    use RepositoryFunctions;

    private EntityManagerInterface $em;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em)
    {
        parent::__construct($registry, ExternalService::class);
        $this->em = $em;
    }

    public function findOneByIdOrNameOrCreate(array $criteria)
    {
        $qb = $this->em->createQueryBuilder();
        $externalService = $qb->select(array('s')) // string 'u' is converted to array internally
        ->from(ExternalService::class, 's')
            ->where($qb->expr()->orX(
                $qb->expr()->eq('s.id', '?1'),
                $qb->expr()->eq('s.name', '?2')
            ))
            ->setParameters(array(1 => $criteria["id"], 2 => $criteria['name']))
            ->getQuery()
            ->getOneOrNullResult(AbstractQuery::HYDRATE_OBJECT);


        if(empty($externalService)){
            $entity = new ExternalService();

            $entity->setName($criteria["name"]);
            $this->save($entity);

            $externalService=$entity;
        }

        return $externalService;
    }
}