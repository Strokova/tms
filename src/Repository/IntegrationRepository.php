<?php


namespace App\Repository;


use App\Entity\Customer;
use App\Entity\ExternalService;
use App\Entity\Integration;
use App\Traits\RepositoryFunctions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Integration|null find($id, $lockMode = null, $lockVersion = null)
 * @method Integration|null findOneBy(array $criteria, array $orderBy = null)
 * @method Integration[]    findAll()
 * @method Integration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Integration|null save($entity)
 * @method Integration|null update($entity)
 */
class IntegrationRepository extends ServiceEntityRepository
{
    use RepositoryFunctions;

    private EntityManagerInterface $em;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em)
    {
        parent::__construct($registry, Integration::class);
        $this->em = $em;
    }

    public function findOneByCustomerAndServices($customer, $service)
    {
        $qb = $this->em->createQueryBuilder();
        return $qb->select('i') // string 'u' is converted to array internally
        ->from(Integration::class, 'i')
            ->join(Customer::class, 'c', 'WITH', "c.id = i.customers AND (c.id = ?1 OR c.uid = ?1)")
            ->join(ExternalService::class, 'es', 'WITH', "es.id = i.externalServices" .
             " AND (es.id = ?2 OR es.name = ?2)")
            ->setParameters(array(1 => $customer, 2 => $service))
            ->getQuery()
            ->getOneOrNullResult(AbstractQuery::HYDRATE_OBJECT);

    }
}