<?php


namespace App\Repository;


use App\Entity\TokenStorage;
use App\Traits\RepositoryFunctions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TokenStorage|null find($id, $lockMode = null, $lockVersion = null)
 * @method TokenStorage|null findOneBy(array $criteria, array $orderBy = null)
 * @method TokenStorage[]    findAll()
 * @method TokenStorage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method TokenStorage|null save($entity)
 * @method TokenStorage|null update($entity)
 */
class TokenStorageRepository extends ServiceEntityRepository
{
    use RepositoryFunctions;

    private EntityManagerInterface $em;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em)
    {
        parent::__construct($registry, TokenStorage::class);
        $this->em = $em;
    }
}